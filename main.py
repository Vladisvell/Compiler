import shutil
from fastapi import FastAPI
from fastapi.responses import FileResponse, JSONResponse
from pydantic import BaseModel
from config import load_config, load_config_from_env
import json

from utils import clone_repo, compile_repo
from database import log_compilation, connect, get_compile_logs

class GitRepo(BaseModel):
    url: str
    build_file: str = "Makefile"
    params: str = ""

class NewConfig(BaseModel):
    host: str
    username: str
    password: str
    db_name: str
    db_port: str    

app = FastAPI()

generated_dirs = []

config = load_config_from_env()
connect(config)
print(config)

@app.post('/')
async def compile_from_git_repo(
    repo: GitRepo 
):
    dir = await clone_repo(repo.url)
    if dir == -1:
        return "error"
    
    output_dir = await compile_repo(dir, repo.build_file, repo.params)
    if output_dir == -1:
        return "error"
        
    log_compilation(config, repo.url)
    generated_dirs.append(output_dir)
    return FileResponse(path=output_dir, filename=output_dir, media_type='application/octet-stream')

@app.get('/')
async def get_last_builds():    
    return JSONResponse(str(get_compile_logs(config)))

@app.delete('/')
async def delete_local_cache():
    for i in generated_dirs:
        shutil.rmtree(i, ignore_errors=True)
    generated_dirs = []
    return JSONResponse("Cache has been cleaned successfully.")

@app.patch('/')
async def update_config(conf: NewConfig):
    config['host'] = conf.host
    config['port'] = conf.db_port    
    config['user'] = conf.username
    config['dbname'] = conf.db_name
    config['password'] = conf.password
    return JSONResponse("New config applied successfully.")

@app.patch('/reset')
async def reset_config():
    config = load_config_from_env() 
    return JSONResponse("Configuration resetted successfully.")