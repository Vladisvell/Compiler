import psycopg2
from config import load_config

def connect(config):
    """ Connect to the PostgreSQL database server """
    try:
        # connecting to the PostgreSQL server
        with psycopg2.connect(**config) as conn:
            #print('Connected to the PostgreSQL server.')
            q = conn.cursor()
            q.execute('SELECT version()')

            print(q.fetchone())

            conn.close()
            return conn
    except (psycopg2.DatabaseError, Exception) as error:
        print(error)

def log_compilation(config, repository_link):
    try:
        # connecting to the PostgreSQL server
        with psycopg2.connect(**config) as conn:
            #print('Connected to the PostgreSQL server.')
            q = conn.cursor()
            q.execute("""CREATE TABLE IF NOT EXISTS compilation_logs ( 
                id SERIAL PRIMARY KEY, 
                repository_link varchar(450) NOT NULL, 
                created_at  TIMESTAMP NOT NULL)""")
            q.execute(f"""INSERT INTO compilation_logs (repository_link, created_at) VALUES ('{repository_link}', current_timestamp)""")            
            conn.commit()
            return conn
    except (psycopg2.DatabaseError, Exception) as error:
        print(error)

def get_compile_logs(config):
    try:
        # connecting to the PostgreSQL server
        with psycopg2.connect(**config) as conn:
            #print('Connected to the PostgreSQL server.')
            q = conn.cursor()
            q.execute("""SELECT * FROM compilation_logs
                        ORDER BY created_at DESC
                        LIMIT 10
                      """)                                    
            return q.fetchall()
    except (psycopg2.DatabaseError, Exception) as error:
        print(error)

if __name__ == '__main__':
    config = load_config()
    #connect(config)
    log_compilation(config, "123")