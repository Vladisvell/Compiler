from subprocess import Popen, PIPE
from uuid import uuid4
import asyncio
import glob
import regex

async def clone_repo(url: str):
    dir = uuid4().hex
    proc = Popen(['git', 'clone', '--progress', str(url), dir], stderr=PIPE, stdout=PIPE)
    
    while True:
        retcode = proc.poll()
        if retcode is None:
            await asyncio.sleep(5)
        else:
            break
    (out,err) = proc.communicate()
    #print("ERROR:")
    #print(err.decode('utf8'))
    if err.decode('utf8').find("fatal:") != -1:
        return -1;
    return dir


async def compile_repo(dir: str, bld_file: str = "Makefile", gcc_param: str = ""):
    files = glob.glob(dir +  "/**/composer.json", recursive=True)
    print(files)
    if len(files) > 0:
        return await compile_compose(dir)

    files = glob.glob(dir +  f"/**/{bld_file}", recursive=True)
    print(files)
    if len(files) > 0:
        return await compile_make(dir, bld_file)
    return await compile_gcc(dir, gcc_param)



async def compile_compose(dir: str):
    task = f"cd {dir}; composer install"
    proc = Popen(task, shell=True, stderr=PIPE, stdout=PIPE)
    
    while True:
        retcode = proc.poll()
        if retcode is None:
            await asyncio.sleep(5)
        else:
            break

    (out,err) = proc.communicate()
    print("OUTPUT:")
    print(out.decode("cp866"))
    print("ERROR:")
    print(err.decode('cp866'))

    task = f"zip -r {dir}.zip {dir}"
    proc = Popen(task, shell=True, stderr=PIPE, stdout=PIPE)
    
    while True:
        retcode = proc.poll()
        if retcode is None:
            await asyncio.sleep(5)
        else:
            break
    (out,err) = proc.communicate()
    print("OUTPUT:")
    print(out.decode("cp866"))
    print("ERROR:")
    print(err.decode('cp866'))
    
    return f"{dir}.zip"

async def compile_make(dir: str, bld_file: str = "Makefile"):
    task = f"cd {dir}; make -f {bld_file}"
    proc = Popen(task, shell=True, stderr=PIPE, stdout=PIPE)
    
    while True:
        retcode = proc.poll()
        if retcode is None:
            await asyncio.sleep(5)
        else:
            break
    (out,err) = proc.communicate()
    print(out.decode("cp866"))
    out_str = regex.search(r"-o \b\w*\b", out.decode("cp866"))
    if out_str is None:
        return -1;
    out_str = out_str[0].split(' ')[1]
    print("ERROR:")
    print(err.decode('cp866'))

    return glob.glob(dir +  f"/**/{out_str}", recursive=True)[0]

async def compile_gcc(dir: str, params: str = ""):
    c_files = glob.glob(dir +  "/**/*.[hc]", recursive=True)
    cpp_files = glob.glob(dir +  "/**/*.[hc]pp", recursive=True)
    print(' '.join(c_files+cpp_files))
    compiler = "g++"
    if len(cpp_files) <= 0:
        compiler = "gcc"
    if len(c_files) <= 0:
        return -1
    
    task = f"{compiler} {params} {' '.join(c_files+cpp_files)} -o out_{dir}"
    proc = Popen(task, shell=True, stderr=PIPE, stdout=PIPE)
    
    while True:
        retcode = proc.poll()
        if retcode is None:
            await asyncio.sleep(5)
        else:
            break
    (out,err) = proc.communicate()
    print("OUTPUT:")
    print(out.decode("cp866"))
    print("ERROR:")
    print(err.decode('cp866'))

    return f"out_{dir}"

