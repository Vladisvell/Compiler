from configparser import ConfigParser
import os

def load_config(filename='database.ini', section='postgresql'):
    parser = ConfigParser()
    parser.read(filename)

    # get section, default to postgresql
    config = {}
    if parser.has_section(section):
        params = parser.items(section)
        for param in params:
            config[param[0]] = param[1]
    else:
        raise Exception('Section {0} not found in the {1} file'.format(section, filename))

    return config

def load_config_from_env():
    config = {}
    config['host'] = os.environ['host']
    config['port'] = os.environ['port']
    config['sslmode'] = os.environ['sslmode']
    config['user'] = os.environ['user']
    config['dbname'] = os.environ['dbname']
    config['password'] = os.environ['password']
    config['target_session_attrs'] = os.environ['target_session_attrs']
    return config    

if __name__ == '__main__':
    config = load_config_from_env()
    print(config)